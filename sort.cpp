#include<iostream>
#include<vector>
#include<cmath>
#include<algorithm>


template <typename d_type>
int sort_check(vector<d_type>& tab)
{
  for(unsigned int i=0;i<tab.size()-1;)
    if(tab.at(i)==tab.at(i+1))
      ++i;
    else 
      if(tab.at(i)>tab.at(i+1))
	{
	  for(;i<tab.size()-1;++i)
	    if(tab.at(i)<tab.at(i+1)) return 0;
	  return -1;
	}
      else 
	if(tab.at(i)<tab.at(i+1))
	  {
	    for(;i<tab.size()-1;++i)
	      if(tab.at(i)>tab.at(i+1)) return 0;
	    return 1;
	  }
  return 4;
}

template <typename d_type>
void podziel(vector<d_type>& source,vector<d_type>& t1,vector<d_type>& t2)
{
  unsigned int i=0;
  t1=vector<d_type>(source.size()/2);
  t2=vector<d_type>(source.size()-source.size()/2);
  for(i=0;i<source.size()/2;++i)
    t1.at(i)=source.at(i);
  for(;i<source.size();++i)
    t2.at(i-source.size()/2)=source.at(i);     
   
}

template <typename d_type>
void scalanie(vector<d_type>& s1,vector<d_type>& s2,vector<d_type>& target,int comp)
{
  int i=0, j=0, n=0;
  while(i<s1.size() && j<s2.size())
    if(comp*s1.at(i)<comp*s2.at(j))
      {
	target.at(n)=s1.at(i);
	++i;
	++n;
      }
    else
      {
	target.at(n)=s2.at(j);
	++j;
	++n;
      }
  for(;i<s1.size();++i,++n)
    target.at(n)=s1.at(i);
  for(;j<s2.size();++j,++n)
    target.at(n)=s2.at(j);    
}

template <typename d_type>
void sort_scalanie(vector<d_type>& tab, int comp)
{
  vector<d_type> t1;
  vector<d_type> t2;
  if(tab.size()>1)
    {
      podziel(tab,t1,t2);
      sort_scalanie(t1,comp);
      sort_scalanie(t2,comp);
      scalanie(t1,t2,tab,comp);
    }
}

template <typename d_type>
int wybierz_pivot(vector<d_type>& tab)
{
  if(tab.size()>3)
    {
      d_type a= tab.at(tab.size()/2-1);
      d_type b= tab.at(tab.size()/2);
      d_type c= tab.at(tab.size()/2+1);
      if(a>b)
	if(a>c)
	  if(b>c)
	    return tab.size()/2;
	  else
	    return tab.size()/2+1;
	else
	  return tab.size()/2-1;
      else
	if(b>c)
	  if(c>a)
	    return tab.size()/2+1;
	  else
	    return tab.size()/2-1;
	else
	  return tab.size()/2;
    }
  return tab.size()/2;
}

template <typename d_type>
void q_podziel(vector<d_type>& source,vector<d_type>& L,vector<d_type>& E,vector<d_type>& G,int pivot,int comp)
{
  for(unsigned int i=0;i<source.size();++i)
    if(comp*source.at(i)<comp*source.at(pivot))
      L.push_back(source.at(i));
    else
      if(source.at(i)==source.at(pivot))
	E.push_back(source.at(i));
      else 
	if(comp*source.at(i)>comp*source.at(pivot))
	  G.push_back(source.at(i));
}

template <typename d_type>
void q_scalanie(vector<d_type>& s1,vector<d_type>& s2,vector<d_type>& s3,vector<d_type>& target)
{
  int i=0, j=0,k=0, n=0;
  for(;i<s1.size();++i,++n)
    target.at(n)=s1.at(i);
  for(;j<s2.size();++j,++n)
    target.at(n)=s2.at(j);    
  for(;k<s3.size();++k,++n)
    target.at(n)=s3.at(k);    
}

template <typename d_type>
void sort_quick(vector<d_type>& tab,int comp)
{
  int pivot;
  vector<d_type> L,E,G;
  if(tab.size()>1)
    {
      pivot=wybierz_pivot(tab);
      q_podziel(tab,L,E,G,pivot,comp);
      sort_quick(L,comp);
      sort_quick(G,comp);
      q_scalanie(L,E,G,tab);
    }
}

template <typename d_type>
void sort_insert(vector<d_type>& tab,int comp)
{
    int temp, j;
   
    for( int i = 1; i < tab.size(); i++ )
    {
      temp = tab.at(i);
       
        for( j = i - 1; j >= 0 && comp*tab[ j ] > comp*temp; j-- )
             tab[ j + 1 ] = tab[ j ];
       
        tab[ j + 1 ] = temp;
    }
}

template <typename d_type>
void sort_hybr_intro(vector<d_type>& tab,int comp)
{
  int M=2*log(tab.size())/M_LN2;
  sort_intro(tab,comp,M);
  sort_insert(tab,comp);
}

template <typename d_type>
void heapify (vector<d_type> tab, int heap_size, int i,int comp)
{
  int largest, temp;
  int l=2*i, r=(2*i)+1;
  if (l<=heap_size && comp*tab[l]>comp*tab[i])
    largest=l;
  else largest=i;
  if (r<=heap_size && comp*tab[r]>comp*tab[largest])
    largest=r;
  if (largest!=i)
    {
      temp=tab[largest];
      tab[largest]=tab[i];
      tab[i]=temp;
      heapify(tab,heap_size,largest,comp);
    }
}

template <typename d_type>
void budKopiec(vector<d_type> tab, int rozmiar,int comp)
{
  for (int i=rozmiar/2;i>0;i--)
    heapify(tab,rozmiar, i,comp);
}

template <typename d_type>
void heap_sort(vector<d_type> tab, int comp)
{
  d_type temp;
  budKopiec(tab, tab.size(),comp);
  for (int i=tab.size();i>1;i--)
    {
      temp=tab[i];
      tab[i]=tab[1];
      tab[1]=temp;
      heapify(tab,tab.size()-1,1,comp);
    }
}

template <typename d_type>
void sort_intro(vector<d_type>& tab,int comp,int M)
{
  int pivot;
  vector<d_type> L,E,G;
  if(M>0)
    {
      if(tab.size()>9)
	{
	  pivot=wybierz_pivot(tab);
	  q_podziel(tab,L,E,G,pivot,comp);
	  sort_intro(L,comp,M-1);
	  sort_intro(G,comp,M-1);
	  q_scalanie(L,E,G,tab);
	}
    }
  else
    sort_heap(tab.begin(),tab.end());
}


