#include <vector>
#include <iostream>
#include <fstream>
#include "Timer.hh"
#include "sort.cpp"
#include "menu.hh"

#define I_TESTOW 100

template <typename d_type>
d_type minimum(vector<d_type>& time_tab)
{
  d_type min;
  for(const d_type& iter: time_tab)
    if(iter<=min)
      min=iter;
  return min;
}

template <typename d_type>
d_type maximum(vector<d_type>& time_tab)
{
  d_type max;
  for(const d_type& iter: time_tab)
    if(iter>=max)
      max=iter;
  return max;
}

template <typename d_type>
d_type mean(vector<d_type>& time_tab)
{
  d_type mean=0;
  for(const d_type& iter: time_tab)
    mean+=iter;
  mean/=time_tab.size();
  return mean;
}




template <typename d_type>
void test1(vector<d_type>& tab,int ilosc,Timer tim1, ofstream& outfile,int promil)
{
  vector<double>scalanie_time(I_TESTOW);
  vector<double>quick_time(I_TESTOW);
  vector<double>intro_time(I_TESTOW);
  vector<vector<d_type>> tabtab(I_TESTOW);
  outfile<<"Losowe wartosci w tablicy."<<(double)promil/10<<"% posortowanej tablicy"<<endl;
  cout<<"Testowanie algorytmow dla "<<(double)promil/10<<"% posortowanej tablicy"<<endl;
  cout<<"Testowanie sortowania tablicy "<<ilosc<<" elementow..."<<endl;
  cout<<"1)Losowanie tablicy"<<endl;
  srand(time(NULL));
  for(unsigned int i=0;i<I_TESTOW;++i)
    {
      if(promil==1000)
	{
	  tabtab.at(i)=vector<d_type>(ilosc);
	  for(d_type& iter: tabtab.at(i))
	    iter=rand()%(ilosc*10);
	  sort_quick(tabtab.at(i),-1);
	}
      else 
	{
	  vector<d_type> buff(ilosc*promil/1000);
	  for(d_type& iter: buff)
	    iter=rand()%(ilosc*promil/100);
	  sort_quick(buff,1);
	  tabtab.at(i)=vector<d_type>(ilosc);
	  for(int iter=0;iter<buff.size();++iter)
	    tabtab.at(i).at(iter)=buff.at(iter);
	  for(int iter=buff.size();iter<tabtab.at(i).size();++iter)
	    tabtab.at(i).at(iter)=rand()%(ilosc*(1000-promil)/100)+ilosc*promil/100;
	}
    }
      /***********/
  cout<<"2)scalanie sort"<<endl;
  for(unsigned int i=0;i<I_TESTOW;++i)
    {
      tim1.start();
      sort_scalanie(tabtab.at(i),1);
      tim1.stop();
      scalanie_time.at(i)=tim1.getElapsedTimeInMilliSec();
    }
      /***********/
  cout<<"3)Quick sort"<<endl;
  for(unsigned int i=0;i<I_TESTOW;++i)
    {
      tim1.start();
      sort_quick(tabtab.at(i),1);
      tim1.stop();
      quick_time.at(i)=tim1.getElapsedTimeInMilliSec();
    }
      /***********/
  cout<<"4)Intro sort"<<endl;
  for(unsigned int i=0;i<I_TESTOW;++i)
    {
      tim1.start();
      sort_hybr_intro(tabtab.at(i),1);
      tim1.stop();
      intro_time.at(i)=tim1.getElapsedTimeInMilliSec();
    }
  cout<<"Zrobione!-----------------------"<<endl;

  outfile<<"Ilosc elementow: "<<ilosc<<endl;
  outfile<<"scalanie. Czas minimalny: "<<minimum(scalanie_time)<<endl;
  outfile<<"scalanie. Czas maksymalny: "<<maximum(scalanie_time)<<endl;
  outfile<<"scalanie. Czas sredni: "<<mean(scalanie_time)<<endl<<endl;

  outfile<<"Quick. Czas minimalny: "<<minimum(quick_time)<<endl;
  outfile<<"Quick. Czas maksymalny: "<<maximum(quick_time)<<endl;
  outfile<<"Quick. Czas sredni: "<<mean(quick_time)<<endl<<endl;

  outfile<<"Intro. Czas minimalny: "<<minimum(intro_time)<<endl;
  outfile<<"Intro. Czas maksymalny: "<<maximum(intro_time)<<endl;
  outfile<<"Intro. Czas sredni: "<<mean(intro_time)<<endl;
  outfile<<"-----------------------"<<endl;
}

template <typename d_type>
void tests(vector<d_type>& tab)
{
  Timer tim1;
  ofstream outfile;
  outfile.open("Testy");
  test1(tab,10000,tim1,outfile,0);
  test1(tab,50000,tim1,outfile,0);
  test1(tab,100000,tim1,outfile,0);
  test1(tab,500000,tim1,outfile,0);
  test1(tab,1000000,tim1,outfile,0);
  /**********/
  test1(tab,10000,tim1,outfile,250);
  test1(tab,50000,tim1,outfile,250);
  test1(tab,100000,tim1,outfile,250);
  test1(tab,500000,tim1,outfile,250);
  test1(tab,1000000,tim1,outfile,250);
  /**********/
  test1(tab,10000,tim1,outfile,500);
  test1(tab,50000,tim1,outfile,500);
  test1(tab,100000,tim1,outfile,500);
  test1(tab,500000,tim1,outfile,500);
  test1(tab,1000000,tim1,outfile,500);
  /**********/
  test1(tab,10000,tim1,outfile,750);
  test1(tab,50000,tim1,outfile,750);
  test1(tab,100000,tim1,outfile,750);
  test1(tab,500000,tim1,outfile,750);
  test1(tab,1000000,tim1,outfile,750);
    /**********/
  test1(tab,10000,tim1,outfile,997);
  test1(tab,50000,tim1,outfile,997);
  test1(tab,100000,tim1,outfile,997);
  test1(tab,500000,tim1,outfile,997);
  test1(tab,1000000,tim1,outfile,997);
  /**********/
  test1(tab,10000,tim1,outfile,1000);
  test1(tab,50000,tim1,outfile,1000);
  test1(tab,100000,tim1,outfile,1000);
  test1(tab,500000,tim1,outfile,1000);
  test1(tab,100000,tim1,outfile,1000);
  cout<<"Testowanie zakonczono*********"<<endl;
}






int main()
{
	    vector<int> tab;
	    tests(tab);
	 
}
